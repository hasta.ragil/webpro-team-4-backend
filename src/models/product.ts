import BaseModel from "./base_model";

export class Product extends BaseModel {
    name: string;
    youtube_link: string;
    description: string;

    static get tableName() {
        return 'products';
    }
}
