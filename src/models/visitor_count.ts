import BaseModel from "./base_model";

export class VisitorCount extends BaseModel {
    static get tableName() {
        return 'visitor_count';
    }
}
