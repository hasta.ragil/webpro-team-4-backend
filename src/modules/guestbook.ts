import {module} from '../decorator/module';
import {del, get, post, put} from '../decorator/route';
import {createModuleLogger} from '../helper/logger';
import {checkToken} from '../middleware/jwt';
import {validator} from '../middleware/validation';
import {Product} from "../models/product";
import {VisitorCount} from "../models/visitor_count";
import {GuestBook} from "../models/guestbook";

const log = createModuleLogger('root');

@module('/guestbook')
export default class GuestBookModule {
    @get('/', [])
    public async get(ctx) {
        ctx.body = await GuestBook.query();
    }
    @post('/', [])
    public async post(ctx) {
        ctx.body = await GuestBook.query().insert(ctx.request.body);
    }
    @del('/:id', [])
    public async del(ctx) {
        ctx.body = await GuestBook.query().delete().where({
            id: ctx.params.id
        });
    }
}
