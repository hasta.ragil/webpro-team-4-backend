import {module} from '../decorator/module';
import {del, get, post, put} from '../decorator/route';
import {createModuleLogger} from '../helper/logger';
import {checkToken} from '../middleware/jwt';
import {validator} from '../middleware/validation';
import {Product} from "../models/product";

const log = createModuleLogger('root');

@module('/products')
export default class ProductModule {
    @get('/', [])
    public async get(ctx) {
        ctx.body = await Product.query();
    }
    @post('/', [])
    public async post(ctx) {
        ctx.body = await Product.query().insert(ctx.request.body);
    }
    @put('/:id', [])
    public async put(ctx) {
        ctx.body = await Product.query().update(ctx.request.body).where({
            id: ctx.params.id
        });
    }
    @del('/:id', [])
    public async del(ctx) {
        ctx.body = await Product.query().delete().where({
            id: ctx.params.id
        });
    }
}
