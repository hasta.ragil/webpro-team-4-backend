import {module} from '../decorator/module';
import {del, get, post, put} from '../decorator/route';
import {createModuleLogger} from '../helper/logger';
import {checkToken} from '../middleware/jwt';
import {validator} from '../middleware/validation';
import {Product} from "../models/product";
import {VisitorCount} from "../models/visitor_count";

const log = createModuleLogger('root');

@module('/visitor_count')
export default class VisitorCountModule {
    @get('/', [])
    public async get(ctx) {
        const res:any = await VisitorCount.query().count();
        ctx.body = res[0];
    }
    @post('/', [])
    public async post(ctx) {
        ctx.body = await VisitorCount.query().insert(ctx.request.body);
    }
}
