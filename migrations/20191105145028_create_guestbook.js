
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('guestbooks', (table) => {
            table.uuid('id').primary();
            table.string('fullname');
            table.string('email');
            table.string('message');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.timestamp('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('guestbooks');
};
