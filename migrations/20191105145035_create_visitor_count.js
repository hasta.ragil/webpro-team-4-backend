
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('visitor_count', (table) => {
            table.uuid('id').primary();
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.timestamp('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('visitor_count');
};
