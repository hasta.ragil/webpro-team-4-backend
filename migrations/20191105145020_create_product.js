
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('products', (table) => {
            table.uuid('id').primary();
            table.string('name');
            table.string('youtube_link');
            table.string('description');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
            table.timestamp('deleted_at').defaultTo(null);
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('products');
};
